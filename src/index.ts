

// max of element
export const max = (args: number[]): number => {
  return args.reduce ((prev: number, curr: number) => (prev > curr ? prev : curr));
}

// to array 
export const toArray = <T> (...coll: T[]): T[] => {
  return  coll.reduce((a: T[], b: T) => [...a, b], []);
}


//unique elements
export const getUnique = <T>(acc: T[], val: T): T[] => {
  if (acc.indexOf(val) === -1) {
    return [...acc, val];
  } else {
    return [...acc];
  }
};



//Reduce
export const myReduce = <T>( init: T, coll: T[]): T => {
  let result: T = init;
  for (const item of coll) {
    result = (result, item);
  }
  return result;
};

export const reduce = <T> (a: T, b: T) => (a+b): T => myReduce; 
test('myReduce<number>( 0, [1, 2, 3, 4]) -> 10', () => {
    expect(myReduce<number>( 0, [1, 2, 3, 4])).toBe(10);
  });