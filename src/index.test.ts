// import { max, toArray, getUnique, uniquee } from './index';

import {
max,
toArray,
Unique
} from './index';
test('Max Of element', () => {
   test('max of -1, 0 and 1', () => {
    expect(max([-1, 0, -3])).toBe(0);
  });
  test('max of -10, -13, - 5 ', () => {
    expect(max([-10, -13, -5])).toBe(-5);
  });
  test('max of 8, 9 and 10', () => {
    expect(max([8, 9, 10])).toBe(10);
  });
  test('max of 5, 6 and 9', () => {
    expect(max([5, 6, 9])).toBe(9);
  });
  test('max of -1, 2 and -7', () => {
    expect(max([-1, 2, -7])).toBe(2);
  });
 
});
test('to Array', () => {
  test('toArray of 1, 2, 3', () => {
    expect(toArray<number>(1, 2, 3)).toEqual([1, 2, 3]);
  });
  test('toArray of a, b and c', () => {
    expect(toArray<string>('a', 'b', 'c')).toEqual(['a', 'b', 'c']);
  });
  test('toArray of ', () => {
    expect(toArray<string>()).toEqual([]);
  });
});
/*
test('unique element',() => {
  test('unique element [1,2,3,4,4,5,6,6,7 ]' , ()=> {
    expect(Unique<number>([1,2,3,4,4,5,6,6,7]).toEqual([1,2,3,4,5,6,7]);
    })
  })
describe('Unique Elements', () => {
  test('getunique([],3)', () => {
    expect(getUnique<number>([], 3)).toEqual([3]);
  });

  test('getunique([7,6],6)', () => {
    expect(getUnique<number>([7, 6], 6)).toEqual([7, 6]);
  });
  */
  

 